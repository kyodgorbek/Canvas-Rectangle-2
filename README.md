# Canvas-Rectangle-2
Rectangle

<div ><canvas id="Canvas2" width="200" height = "200" style="border:solid 1px #000000;"></canvas>
   <div>
     <button onclick="blue_square_2();return true;">Blue Square</button>
     <button onclick="red_stroke_2();return true;">Red Square</button>
     <button onclick="clear_rect_2();return true;">Erase Everything</button>
   </div> 
</div>
<script>
   var c2 = document.getElementById("c2");
   var c2_context = c2.getContext("2d");
   function blue_square_2() { //Blue color square
     c2_context.fillStyle = "#00f";
     c2_context.fillRect(50, 50, 100, 100);
   }
   function red_stroke_2() { //Red color edges
     c2_context.strokeStyle = "#f00";
     c2_context.strokeRect(45, 45, 110, 110);
   }
   function clear_rect_2() { //Clear all
     c2_context.clearRect(40, 40, 120, 120);
   }
</script>
